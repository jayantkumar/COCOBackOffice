﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Entity.Data;
using Entity.Entities;

namespace CocoWebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/ProductNutrition")]
    public class ProductNutritionController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ProductNutritionController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/ProductNutrition
        [HttpGet]
        public IEnumerable<ProductNutrition> GetProductNutrition()
        {
            return _context.ProductNutrition;
        }

        // GET: api/ProductNutrition/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetProductNutrition([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var productNutrition = await _context.ProductNutrition.SingleOrDefaultAsync(m => m.Id == id);

            if (productNutrition == null)
            {
                return NotFound();
            }

            return Ok(productNutrition);
        }

        // PUT: api/ProductNutrition/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutProductNutrition([FromRoute] long id, [FromBody] ProductNutrition productNutrition)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != productNutrition.Id)
            {
                return BadRequest();
            }

            _context.Entry(productNutrition).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProductNutritionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ProductNutrition
        [HttpPost]
        [Route("api/ProductNutin/AddPrdctNutrin")]
        public async Task<IActionResult> PostProductNutrition([FromBody] ProductNutrition productNutrition)
        {
            try
            {
                _context.ProductNutrition.Add(productNutrition);
                await _context.SaveChangesAsync();

            }
            catch (Exception ex)
            {

            }
           
            return CreatedAtAction("GetProductNutrition", new { id = productNutrition.Id }, productNutrition);
        }

        // DELETE: api/ProductNutrition/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteProductNutrition([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var productNutrition = await _context.ProductNutrition.SingleOrDefaultAsync(m => m.Id == id);
            if (productNutrition == null)
            {
                return NotFound();
            }

            _context.ProductNutrition.Remove(productNutrition);
            await _context.SaveChangesAsync();

            return Ok(productNutrition);
        }

        private bool ProductNutritionExists(long id)
        {
            return _context.ProductNutrition.Any(e => e.Id == id);
        }
    }
}
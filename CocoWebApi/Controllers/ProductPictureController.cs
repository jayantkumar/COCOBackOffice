﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Entity.Data;
using Entity.Entities;

namespace CocoWebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/ProductPicture")]
    public class ProductPictureController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ProductPictureController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/ProductPicture
        [HttpGet]
        public IEnumerable<ProductPicture> GetProductPicture()
        {
            return _context.ProductPicture;
        }

        // GET: api/ProductPicture/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetProductPicture([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var productPicture = await _context.ProductPicture.SingleOrDefaultAsync(m => m.Id == id);

            if (productPicture == null)
            {
                return NotFound();
            }

            return Ok(productPicture);
        }

        // PUT: api/ProductPicture/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutProductPicture([FromRoute] long id, [FromBody] ProductPicture productPicture)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != productPicture.Id)
            {
                return BadRequest();
            }

            _context.Entry(productPicture).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProductPictureExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ProductPicture
        [HttpPost]
        [Route("api/ProductPicture/AddPicture")]
        public async Task<IActionResult> PostProductPicture([FromBody] ProductPicture productPicture)
        {
            try
            {
                _context.ProductPicture.Add(productPicture);
                await _context.SaveChangesAsync();
            }
            catch(Exception ex)
            {

            }
           
            return CreatedAtAction("GetProductPicture", new { id = productPicture.Id }, productPicture);
        }

        // DELETE: api/ProductPicture/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteProductPicture([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var productPicture = await _context.ProductPicture.SingleOrDefaultAsync(m => m.Id == id);
            if (productPicture == null)
            {
                return NotFound();
            }

            _context.ProductPicture.Remove(productPicture);
            await _context.SaveChangesAsync();

            return Ok(productPicture);
        }

        private bool ProductPictureExists(long id)
        {
            return _context.ProductPicture.Any(e => e.Id == id);
        }
    }
}
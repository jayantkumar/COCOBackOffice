﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Entity.Data;
using Entity.Entities;

namespace CocoWebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Products")]
    public class ProductsController : Controller
    {
        private readonly ApplicationDbContext _context;
        
        public ProductsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/Products
        [HttpGet]
        public IActionResult GetProduct(string param)
        {
            List<Product> prodcts = new List<Product>();
            List<ProductPicture> productPicture = new List<ProductPicture>();
            List<ProductNutrition> productNutrition = new List<ProductNutrition>();
            List<Unit> unit = new List<Unit>();
            prodcts = _context.Product.ToList();
            if (param == "Product")
            {

                prodcts = _context.Product.ToList();
                return PartialView("_ProductDetails", prodcts);
            }
            else if (param == "ProductPic")
            {

                productPicture = _context.ProductPicture.ToList();
                return PartialView("ProdctPicDetails", productPicture);
            }
            else if (param == "ProductNutrition")
            {

                productNutrition = _context.ProductNutrition.ToList();
                return PartialView("_ProdctNutrinDetails", productNutrition);
            }
            else if (param == "ProductUnit")
            {

                unit = _context.Unit.ToList();
                return PartialView("_ProdctUnitDetails", unit);
            }


            return PartialView("_ProductDetails", prodcts);
        }

        // GET: api/Products/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetProduct([FromRoute] long id)
        {
           
            var product = await _context.Product.SingleOrDefaultAsync(m => m.Id == id);

            if (product == null)
            {
                return NotFound();
            }

            return Ok(product);
        }

        // PUT: api/Products/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutProduct([FromRoute] long id, [FromBody] Product product)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != product.Id)
            {
                return BadRequest();
            }

            _context.Entry(product).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProductExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Products
        [HttpPost]
        [Route("InsertProduct")]
        public async Task<IActionResult> PostProduct([FromBody] Product product)
        {
            try
            {
                _context.Product.Add(product);
                await _context.SaveChangesAsync();
            }
            catch(Exception ex)
            {

            }

            return CreatedAtAction("GetProduct", new { id = product.Id }, product);
        }

        // DELETE: api/Products/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteProduct([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var product = await _context.Product.SingleOrDefaultAsync(m => m.Id == id);
            if (product == null)
            {
                return NotFound();
            }

            _context.Product.Remove(product);
            await _context.SaveChangesAsync();

            return Ok(product);
        }

        private bool ProductExists(long id)
        {
            return _context.Product.Any(e => e.Id == id);
        }
    }
}
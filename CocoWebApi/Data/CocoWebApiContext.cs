﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Entity.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace CocoWebApi.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext (DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
        }

        public DbSet<Product> Product { get; set; }

        public DbSet<Entity.Entities.ProductPicture> ProductPicture { get; set; }

        public DbSet<Entity.Entities.PublicNotification> PublicNotification { get; set; }

        public DbSet<Entity.Entities.ProductNutrition> ProductNutrition { get; set; }

        public DbSet<Entity.Entities.Unit> Unit { get; set; }

        public DbSet<Entity.Entities.Category> Category { get; set; }
    }
}

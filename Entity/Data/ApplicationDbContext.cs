﻿using Entity.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Entity.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
        }
        //master model
        public DbSet<StatusMaster> StatusMaster { get; set; }

        public DbSet<NutritionMaster> NutritionMaster { get; set; }

        public DbSet<ModeOfPaymentMaster> ModeOfPaymentMaster { get; set; }

        public DbSet<AppKeyConfig> AppKeyConfig { get; set; }

        public DbSet<PaymentStatusMaster> PaymentStatusMaster { get; set; }

        public DbSet<Category> Category { get; set; }

        public DbSet<ApplicationUserPhoto> ApplicationUserPhoto { get; set; }

        //public DbSet<LoginResponseMessage> LoginResponseMessage { get; set; }

        public DbSet<CoupenTokenMaster> CoupenTokenMaster { get; set; }



        //communication model
        public DbSet<Feedback> Feedback { get; set; }

        public DbSet<ProductReview> ProductReview { get; set; }

        public DbSet<Notification> Notification { get; set; }

        public DbSet<PublicNotification> PublicNotification { get; set; }

        public DbSet<Message> Message { get; set; }

        public DbSet<Invite> Invite { get; set; }

        //cultrue model

        public DbSet<Culture> Culture { get; set; }

        //Delivery model

        public DbSet<DeliveryAddress> DeliveryAddress { get; set; }

        public DbSet<DeliveryDetails> DeliveryDetails { get; set; }

        public DbSet<DeliveryStatus> DeliveryStatus { get; set; }

        public DbSet<Inventory> Inventory { get; set; }

        public DbSet<ProductStock> ProductStock { get; set; }

        //Offer model

        public DbSet<Offer> Offer { get; set; }

        public DbSet<ProductOffer> ProductOffer { get; set; }

        public DbSet<CategoryOffer> CategoryOffer { get; set; }

        public DbSet<SeasonalOffer> SeasonalOffer { get; set; }

        public DbSet<Coupens> Coupens { get; set; }

        public DbSet<CoupenUserTable> CoupenUserTable { get; set; }

        //order model

        public DbSet<Cart> Cart { get; set; }

        public DbSet<OrderTransaction> OrderTransaction { get; set; }

        public DbSet<Invoice> Invoice { get; set; }

        public DbSet<Order> Order { get; set; }

        public DbSet<OrderItem> OrderItem { get; set; }

        public DbSet<CartItem> CartItem { get; set; }

        //product model

        public DbSet<Product> Product { get; set; }

        public DbSet<ProductPicture> ProductPicture { get; set; }

        public DbSet<ProductNutrition> ProductNutrition { get; set; }

        public DbSet<Combo> Combo { get; set; }

        //public DbSet<ComboItem> ComboItem { get; set; }

        public DbSet<Unit> Unit { get; set; }

        public DbSet<ProductPricePerUnit> ProductPricePerUnit { get; set; }

        //Resource model

        public DbSet<Resource> Resource { get; set; }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Entity.Entities
{
    public interface IEntityBase
    {
        long Id { get; set; }
    }

    public interface IEntityEntry
    {
        DateTime EntryDate { get; set; }
    }

    public interface IEntityStatus
    {
        long Status { get; set; }
    }
    public interface IEntityActive
    {
        bool Active { get; set; }
    }
}

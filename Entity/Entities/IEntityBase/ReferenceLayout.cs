﻿using Entity.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Entity.Entities
{

    public interface IEntityUserRef
    {
        string ApplicationUser { get; set; }
        ApplicationUser User { get; set; }
    }

    public interface IEntityProductRef
    {
        long ProductId { get; set; }
        Product Product { get; set; }
    }

    public interface IEntityProductUnitPriceRef
    {
        long ProductPriceId { get; set; }
        ProductPricePerUnit ProductPrice { get; set; }
    }
    public interface IEntityCategoryRef
    {
        int CategoryId { get; set; }
        Category Category { get; set; }
    }

    public interface IEntityUnitRef
    {
        long UnitId { get; set; }
        Unit Unit { get; set; }
    }

}

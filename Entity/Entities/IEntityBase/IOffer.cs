﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Entity.Entities
{
    public interface IOffer
    {
        int OfferType { get; set; }
        decimal LimitUpto { get; set; }
        int DiscountType { get; set; }
        decimal Discount { get; set; }
        DateTime StartDate { get; set; }
        DateTime EndDate { get; set; }
    }

    public enum DiscountType
    {
        Amount=1,
        Percentage=2
    }
    public enum OfferTypes
    {
        OnQuantity = 1,
        OnPrice = 2
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Entity.Entities;
using System.ComponentModel.DataAnnotations;

namespace Entity.Entities
{
    public class Product : IEntityBase, IEntityStatus, IEntityCategoryRef, IEntityEntry,IEntityUserRef
    {
        [Key]
        public long Id { get; set; }
        public string ProductName { get; set; }
        public string ProductDescription { get; set; }
        [ForeignKey("Category")]
        public int CategoryId { get; set; }
        [ForeignKey("StatusDetails")]
        public long Status { get; set; }
        public int Views { get; set; }
        [ForeignKey("User")]
        public string ApplicationUser { get; set; }
        public DateTime EntryDate { get; set; }
        public Product() { this.EntryDate = DateTime.Now; }
        public virtual ApplicationUser User { get; set; }
        public virtual Category Category { get; set; }
        public virtual StatusMaster StatusDetails { get; set; }
        public virtual IEnumerable<ProductPicture> ProductPicture { get; set; }
        public virtual IEnumerable<ProductPricePerUnit> ProductPricePerUnit { get;set;}
    }

    public class ProductPicture:IEntityBase,IEntityProductRef
    {
        [Key]
        public long Id { get; set; }
        [ForeignKey("Product")]
        public long ProductId { get; set; }
        public string PictureType { get; set; }
        public byte[] Picture { get; set; }
        public virtual Product Product { get; set; }
    }

    public class ProductNutrition:IEntityBase,IEntityProductRef
    {
        [Key]
        public long Id { get; set; }
        [ForeignKey("Product")]
        public long ProductId { get; set; }
        [ForeignKey("Nutrition")]
        public int NutritionType { get; set; }
        public string NutritionValue { get; set; }
        public virtual Product Product { get; set; }
        public virtual NutritionMaster Nutrition { get; set; }
    }

    public class Combo
    {
        [Key]
        public long Id { get; set; }
        public int ComboPrice { get; set; }
        public string ComboName { get; set; }
        public byte[] Picture { get; set; }
        public int views { get; set; }
        public Combo() { this.AddDate = DateTime.Now; }
        public DateTime AddDate { get; private set; }
        [ForeignKey("StatusDetails")]
        public long Status { get; set; }
        public virtual StatusMaster StatusDetails { get; set; }
    }



    //public class ComboItem
    //{
    //    [Key]
    //    public long Id { get; set; }
    //    [ForeignKey("Combo")]
    //    public long ComboId { get; set; }
    //   // [ForeignKey("Product")]
    //    public long ProductId { get; set; }
    //    [ForeignKey("Unit")]
    //    public long UnitId { get; set; }
    //    public ComboItem() { this.AddDate = DateTime.Now; }
    //    public DateTime AddDate { get; private set; }
    //    [ForeignKey("StatusDetails")]
    //    public long Status { get; set; }
    //    public virtual StatusMaster StatusDetails { get; set; }
    //    public virtual Combo Combo { get; set; }
    //    //public virtual Product Product { get; set; }
    //    public virtual Unit Unit { get; set; }
    //}

    public class Unit
    {
        [Key]
        public long Id { get; set; }
        public string Title { get; set; }
    }

    public class ProductPricePerUnit
    {
        [Key]
        public long Id { get; set; }
        [ForeignKey("Product")]
        public long ProductId { get; set; }
        [ForeignKey("Unit")]
        public long UnitId { get; set; }
        public decimal Quantity { get; set; }
        public decimal Price { get; set; }
        public virtual Product Product { get; set; }
        public virtual Unit Unit { get; set; }
    }
}

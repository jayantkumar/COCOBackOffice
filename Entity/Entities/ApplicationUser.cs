﻿using Microsoft.AspNetCore.Identity;

namespace Entity.Entities
{
    public class ApplicationUser : IdentityUser
    {
    }
}
﻿using Entity.Entities;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Entity.Entities
{
    public class ApplicationRole : IdentityRole
    {
        [Key]
        public int Id { get; set; }
        [StringLength(250)]
        public string Description { get; set; }

    }

    public class ApplicationUserPhoto : IEntityBase
    {

        [Key]
        public long Id { get; set; }
        public string ContentType { get; set; }
        public byte[] Content { get; set; }
        public string ApplicationUserId { get; set; }
        public ApplicationUser ApplicationUser { get; set; }
    }
}

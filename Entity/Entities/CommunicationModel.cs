﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Entity.Entities;
using System.ComponentModel.DataAnnotations;

namespace Entity.Entities
{
    public class Feedback:IEntityBase,IEntityEntry
    {
        [Key]
        public long Id { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
        [ForeignKey("User")]
        public string UserId { get; set; }
        public DateTime EntryDate { get; set; }
        public virtual ApplicationUser User { get; set; }
    }

    public class ProductReview:IEntityBase,IEntityEntry,IEntityProductRef,IEntityUserRef
    {
        [Key]
        public long Id { get; set; }
        [ForeignKey("Product")]
        public long ProductId { get; set; }
        public string Review { get; set; }
        [ForeignKey("User")]
        public string ApplicationUser { get; set; }
        public int StarRating { get; set; }
        public DateTime EntryDate { get; set; }
        public ProductReview() { this.EntryDate = DateTime.Now; }
        public bool Active { get; set; }
        public virtual ApplicationUser User { get; set; }
        public virtual Product Product { get; set; }
    }

    
    public class Notification:IEntityEntry,IEntityBase,IEntityUserRef
    {
        [Key]
        public long Id { get; set; }
        public DateTime EntryDate { get; set; }
        public Notification() { EntryDate = DateTime.Now; }
        [ForeignKey("User")]
        public string ApplicationUser{ get; set; }
        public string NoteMessage { get; set; }
        public string NoteUrl { get; set; }
        public bool Read { get; set; }
        public string Icon { get; set; }
        public virtual ApplicationUser User { get; set; }
    }

    public class PublicNotification: IEntityBase, IEntityActive,IEntityEntry
    {
        [Key]
        public long Id { get; set; }
        public DateTime EntryDate { get; set; }
        public string NoteMessage { get; set; }
        public string NoteUrl { get; set; }
        public string Icon { get; set; }
        public byte[] Picture { get; set; }
        public bool Active { get; set; }
    }

    public class Message: IEntityBase,IEntityEntry,IEntityUserRef
    {
        [Key]
        public long Id { get; set; }
        public string MessageString { get; set; }
        public string ToId { get; set; }
        [ForeignKey ("User")]
        public string ApplicationUser { get; set; }
        public Message() { this.EntryDate = DateTime.Now; }
        public DateTime EntryDate { get;  set; }
        public int ParentId { get; set; }
        public virtual ApplicationUser User { get; set; }
    }

    public class Invite:IEntityBase
    {
        [Key]
        public long Id { get; set; }
        public string Email { get; set; }
    }



}

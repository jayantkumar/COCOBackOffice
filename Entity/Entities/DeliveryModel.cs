﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Entity.Entities;
using System.ComponentModel.DataAnnotations;

namespace Entity.Entities
{
    public class DeliveryAddress:IEntityBase,IEntityUserRef,IEntityActive,IEntityEntry
    {
        [Key]
        public long Id { get; set; }
        [ForeignKey ("User")]
        public string ApplicationUser { get; set; }
        public string LandMark { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string PinCode { get; set; }
        public string AddressType { get; set; }
        public string AddressLineOne { get; set; }
        public string AddressLineTwo { get; set; }
        public string longitude { get; set; }
        public string latitude { get; set; }
        public bool Active { get; set; }
        public DateTime EntryDate { get; set; }
        public DeliveryAddress() { this.EntryDate = DateTime.Now; }
        public virtual ApplicationUser User { get; set; }
    }

    public class DeliveryDetails:IEntityBase,IEntityActive,IEntityUserRef
    {
        [Key]
        public long Id { get; set; }
        public int OrderId { get; set; }
        [ForeignKey("User")]
        public string DeliveryAssignedTo { get; set; }
        public DateTime DeliveryAssignedTime { get; set; }
        public string ApplicationUser { get; set; }
        public bool Active { get; set; }
        public virtual ApplicationUser User { get; set; }
    }

    public class DeliveryStatus:IEntityBase,IEntityStatus
    {
        [Key]
        public long Id { get; set; }
        [ForeignKey("DeliveryDetails")]
        public long DeliveryId { get; set; }
        public long Status { get; set; }
        public string CurrentLocation { get; set; }
        public DateTime StatusDate { get; set; }
        public string Remark { get; set; }
        public DateTime ExpectedDelivery { get; set; }
        public virtual DeliveryDetails DeliveryDetails { get; set; }
    }


    public class Inventory:IEntityBase,IEntityEntry,IEntityProductRef,IEntityUserRef,IEntityUnitRef
    {
        [Key]
        public long Id { get; set; }
        [ForeignKey("Product")]
        public long ProductId { get; set; }
        public int ProductQuantity { get; set; }
        public decimal ProductPrice { get; set; }

        // Foreign Key Mapped to Vendor Id not with Application User
        [ForeignKey("User")]
        public string VendorId { get; set; }

        // Not binding Foreign Key  [ForeignKey("User")]
        public string ApplicationUser { get; set; }


        [ForeignKey("Unit")]
        public long UnitId { get; set; }
        public DateTime EntryDate { get; set; }
        public Inventory() { this.EntryDate = DateTime.Now; }
        public virtual Product Product { get; set; }
        public virtual ApplicationUser User { get; set; }
        public virtual Unit Unit { get;set;}
    }

    public class ProductStock:IEntityBase,IEntityProductRef,IEntityUnitRef,IEntityEntry
    {
        [Key]
        public long Id { get; set; }
        [ForeignKey("Product")]
        public long ProductId { get; set; }
        public decimal Stock { get; set; }
        [ForeignKey("Unit")]
        public long UnitId { get; set; }
        public DateTime EntryDate { get; set; }
        public virtual Product Product { get; set; }
        public virtual Unit Unit { get; set; }
    }
}

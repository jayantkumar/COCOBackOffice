﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Entity.Entities;
namespace Entity.Entities
{
    public class Cart:IEntityStatus
    {
        [Key]
        public string CartId { get; set; }
        public DateTime EntryDate { get; set; }
        [ForeignKey("StatusMaster")]
        public long Status { get; set; }
        public virtual StatusMaster StatusMaster { get; set; }
    }
    public class OrderTransaction:IEntityBase
    {
        [Key]
        public long Id { get; set; }
        [ForeignKey("Order")]
        public long OrderId { get; set; }
        public float TransactionAmount { get; set; }
        public int TransactionStatus { get; set; }
        public string TransactionReferenceNumber { get; set; }
        public string TransactionRequestString { get; set; }
        public string TransactionResponseString { get; set; }
        public string TransactionSignature { get; set; }
        [ForeignKey("ModeOfPaymentMaster")]
        public int ModeOfPayment { get; set; }
        [ForeignKey("PaymentStatusMaster")]
        public int PaymentStatus { get; set; }
        public DateTime Adddate { get; set; }
        public OrderTransaction() { this.Adddate = DateTime.Now; }
        public virtual ModeOfPaymentMaster ModeOfPaymentMaster { get;set;}
        public virtual PaymentStatusMaster PaymentStatusMaster { get; set; }
        public virtual Order Order { get; set; }
    }

    public class Invoice:IEntityBase
    {
        [Key]
        public long Id { get; set; }
        public string InvoiceNumber { get; set; }
        [ForeignKey("Order")]
        public long OrderId { get; set; }
        public DateTime InvoiceDate { get; set; }
        public Invoice() { this.InvoiceDate = DateTime.Now; }
        public string InvoiceNote { get; set; }
        public string TaxRate { get; set; }
        public string TaxAmount { get; set; }
        public int DiscountPercentage { get; set; }
        public float DiscountAmount { get; set; }
        public float GrandTotal { get; set; }
        public float ShippingAmount { get; set; }
        public string AppliedCoupen { get; set; }
        public float TotalPayableAmount { get; set; }
        public int Active { get; set; }
        public virtual Order Order { get; set; }
    }

    public class Order:IEntityBase
    {
        [Key]
        public long Id { get; set; }
        [ForeignKey("User")]
        public string UserName { get; set; }
        [ForeignKey("Cart")]
        public string CartId { get; set; }
        public int DiscountPercentage { get; set; }
        public string OrderMessage { get; set; }
        [ForeignKey("DeliveryAddress")]
        public long DeliveryAddressId { get; set; }
        public decimal DeliveryCharges { get; set; }
        public decimal DiscountedAmount { get; set; }
        public decimal OriginalAmount { get; set; }
        public int Total { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public System.DateTime OrderDate { get; set; }
        public Order()
        {
            OrderDate = DateTime.Now;
            Active = 0;
        }
        [ForeignKey("CoupenTokenMaster")]
        public int CoupenToken { get; set; }
        public int Active { get; set; }
        public virtual Cart Cart { get; set; }
        public virtual ApplicationUser User { get; set; }
        public virtual DeliveryAddress DeliveryAddress { get; set; }
        public virtual CoupenTokenMaster CoupenTokenMaster { get; set; }
    }

    public class OrderItem: IEntityBase, IEntityProductRef, IEntityUserRef, IEntityProductUnitPriceRef
    {
        [Key]
        public long Id { get; set; }
        [ForeignKey("Order")]
        public long OrderId { get; set; }
        [ForeignKey("Product")]
        public long ProductId { get; set; }
        public int Count { get; set; }
        [ForeignKey("User")]
        public string ApplicationUser { get; set; }
       // [ForeignKey("ProductPrice")]
        public long ProductPriceId { get; set; }
        [ForeignKey("StatusMaster")]
        public long Status { get; set; }
        [DataType(DataType.DateTime)]
        public DateTime DateCreated { get; set; }
        public OrderItem() { this.DateCreated = DateTime.Now; }
        public Decimal TotalPrice { get; set; }
        public virtual Product Product { get; set; }
        public virtual ApplicationUser User { get; set; }
        public virtual ProductPricePerUnit ProductPrice { get; set; }
        public virtual StatusMaster StatusMaster { get; set; }
        public virtual Order Order { get; set; }
    }


    public class CartItem:IEntityBase,IEntityProductRef,IEntityUserRef,IEntityProductUnitPriceRef
    {
        [Key]
        public long Id { get; set; }
        [ForeignKey("Cart")]
        public string CartId { get; set; }
        [ForeignKey("Product")]
        public long ProductId { get; set; }
        public int Count { get; set; }
        [ForeignKey("User")]
        public string ApplicationUser { get; set; }
       // [ForeignKey("ProductPrice")]
        public long ProductPriceId { get; set; }
        [DataType(DataType.DateTime)]
        public DateTime DateCreated { get; set; }
        public CartItem() { this.DateCreated = DateTime.Now; }
        public Decimal TotalPrice { get; set; }
        public virtual Product Product { get; set; }
        public virtual ApplicationUser User { get; set; }
        public virtual ProductPricePerUnit ProductPrice { get; set; }
        public virtual Cart Cart { get; set; }
    }









}

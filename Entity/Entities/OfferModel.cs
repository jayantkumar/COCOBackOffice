﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Entity.Entities;
namespace Entity.Entities
{

    public class Offer:IEntityBase,IEntityEntry
    {
        [Key]
        public long Id { get; set; }
        public string Title { get; set; }
        public byte[] Picture { get; set; }
        public System.DateTime EntryDate { get; set; }
        public Offer()
        {
            EntryDate = DateTime.Now;
            Active = 0;
        }
        public int Active { get; set; }
        [ForeignKey("OfferCategory")]
        public int OfferType { get; set; }
        public virtual OfferCategory OfferCategory { get; set; }
    }

    public enum OfferCategory
    {
        Seasonal = 1,
        Product = 2,
        Category = 3
    }

    public class ProductOffer:IEntityBase,IEntityProductRef,IOffer
    {
        [Key]
        public long Id { get; set; }
        [ForeignKey("Product")]
        public long ProductId { get; set; }
        public decimal LimitUpto { get; set; }
        public decimal Discount { get; set; }
        public int OfferType { get; set; }
        public int DiscountType { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        [ForeignKey("Offer")]
        public long OfferId { get; set; }
        public virtual Offer Offer { get; set; }
        public virtual Product Product { get; set; }
    }

    public class CategoryOffer:IEntityBase,IEntityCategoryRef,IOffer
    {
        [Key]
        public long Id { get; set; }
        [ForeignKey("Category")]
        public int CategoryId { get; set; }
        public decimal LimitUpto { get; set; }
        public decimal Discount { get; set; }
        public int OfferType { get; set; }
        public int DiscountType { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        [ForeignKey("Offer")]
        public long OfferId { get; set; }
        public virtual Offer Offer { get; set; }
        public virtual Category Category { get; set; }
    }

    public class SeasonalOffer:IEntityBase, IOffer
    {
        [Key]
        public long Id { get; set; }
        public decimal LimitUpto { get; set; }
        public decimal Discount { get; set; }
        public int OfferType { get; set; }
        public int DiscountType { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        [ForeignKey("Offer")]
        public long OfferId { get; set; }
        public virtual Offer Offer { get; set; }
    }

    public class Coupens:IEntityBase,IEntityEntry,IEntityActive
    {
        [Key]
        public long Id { get; set; }
        [Remote("IsCoupenExists", "Coupens", ErrorMessage = "Coupen code already in use")]
        public string CoupenCode { get; set; }
        public bool Active { get; set; }
        public string DiscountPercentage { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime ExpiryDate { get; set; }
        public DateTime EntryDate { get;  set; }
        public int CoupenFor { get; set; }
    }

    public class CoupenUserTable:IEntityBase,IEntityUserRef,IEntityEntry
    {
        [Key]
        public long Id { get; set; }
        public string CoupenCode { get; set; }
        [Remote("IsUserExists", "CoupenUserTables", ErrorMessage = "Coupen already assign to this user")]
        [ForeignKey("User")]
        public string ApplicationUser { get; set; }
        public int ApplyStatus { get; set; }
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime? ExpiryDate { get; set; }
        public DateTime EntryDate { get; set; }
        public virtual ApplicationUser User { get; set; }
    }

  
}

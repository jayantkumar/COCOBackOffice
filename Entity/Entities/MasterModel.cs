﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Entity.Entities;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Entity.Entities
{
    public class AppKeyConfig
    {
        [Key]
        public long Id { get; set; }
        public string BulkSmsUrl { get; set; }
        public string BulkSmsUser { get; set; }
        public string BulkSmsPassword { get; set; }
        public string BulkSmsSid { get; set; }
    }

    public class LoginResponseMessage
    {
        
        public string Status { get; set; }
        public string Message { get; set; }
        public string CartId { get; set; }
    }

    public class StatusMaster:IEntityBase
    {
        [Key]
        public long Id { get; set; }
        public string Title { get; set; }
        public bool Active { get; set; }
    }

    public class NutritionMaster
    {
        [Key]
        public int Id { get; set; }
        public string Title { get; set; }
        public bool Active { get; set; }
    }

    public class PaymentStatusMaster
    {
        [Key]
        public int Id { get; set; }
        public string Title { get; set; }
        public bool Active { get; set; }
    }

    public class CoupenTokenMaster
    {
        [Key]
        public int Id { get; set; }
        public string Title { get; set; }
        public bool Active { get; set; }
    }

    public class ModeOfPaymentMaster
    {
        [Key]
        public int Id { get; set; }
        public string Title { get; set; }
        public bool Active { get; set; }
    }

    public class Category
    {
        [Key]
        public int Id { get; set; }
        [Required(ErrorMessage = "This field can not be empty.")]
        public string CategoryTitle { get; set; }
        [ForeignKey("CategoryDetails")]
        public int? ParentId { get; set; }
        public virtual Category CategoryDetails { get; set; }
        public IList<Category> ChildMenu { get; set; }
        
    }
    public class BindParents
    {
        public SelectList BindList { get; set; }
    }

}
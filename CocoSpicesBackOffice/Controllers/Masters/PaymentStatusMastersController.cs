﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Entity.Entities;
using Entity.Data;
namespace CocoSpicesBackOffice.Controllers.Masters
{
    public class PaymentStatusMastersController : Controller
    {
        private readonly ApplicationDbContext _context;

        public PaymentStatusMastersController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: PaymentStatusMasters
        public async Task<IActionResult> Index()
        {
            return View(await _context.PaymentStatusMaster.ToListAsync());
        }

        // GET: PaymentStatusMasters/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var paymentStatusMaster = await _context.PaymentStatusMaster
                .SingleOrDefaultAsync(m => m.Id == id);
            if (paymentStatusMaster == null)
            {
                return NotFound();
            }

            return View(paymentStatusMaster);
        }

        // GET: PaymentStatusMasters/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: PaymentStatusMasters/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Title,Active")] PaymentStatusMaster paymentStatusMaster)
        {
            if (ModelState.IsValid)
            {
                _context.Add(paymentStatusMaster);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(paymentStatusMaster);
        }

        // GET: PaymentStatusMasters/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var paymentStatusMaster = await _context.PaymentStatusMaster.SingleOrDefaultAsync(m => m.Id == id);
            if (paymentStatusMaster == null)
            {
                return NotFound();
            }
            return View(paymentStatusMaster);
        }

        // POST: PaymentStatusMasters/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Title,Active")] PaymentStatusMaster paymentStatusMaster)
        {
            if (id != paymentStatusMaster.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(paymentStatusMaster);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PaymentStatusMasterExists(paymentStatusMaster.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(paymentStatusMaster);
        }

        // GET: PaymentStatusMasters/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var paymentStatusMaster = await _context.PaymentStatusMaster
                .SingleOrDefaultAsync(m => m.Id == id);
            if (paymentStatusMaster == null)
            {
                return NotFound();
            }

            return View(paymentStatusMaster);
        }

        // POST: PaymentStatusMasters/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var paymentStatusMaster = await _context.PaymentStatusMaster.SingleOrDefaultAsync(m => m.Id == id);
            _context.PaymentStatusMaster.Remove(paymentStatusMaster);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PaymentStatusMasterExists(int id)
        {
            return _context.PaymentStatusMaster.Any(e => e.Id == id);
        }
    }
}

﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Entity.Entities;
using Entity.Data;
namespace CocoSpicesBackOffice.Controllers.Masters
{
    public class AppKeyConfigsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public AppKeyConfigsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: AppKeyConfigs
        public async Task<IActionResult> Index()
        {
            return View(await _context.AppKeyConfig.ToListAsync());
        }

        // GET: AppKeyConfigs/Details/5
        public async Task<IActionResult> Details(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var appKeyConfig = await _context.AppKeyConfig
                .SingleOrDefaultAsync(m => m.Id == id);
            if (appKeyConfig == null)
            {
                return NotFound();
            }

            return View(appKeyConfig);
        }

        // GET: AppKeyConfigs/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: AppKeyConfigs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,BulkSmsUrl,BulkSmsUser,BulkSmsPassword,BulkSmsSid")] AppKeyConfig appKeyConfig)
        {
            if (ModelState.IsValid)
            {
                _context.Add(appKeyConfig);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(appKeyConfig);
        }

        // GET: AppKeyConfigs/Edit/5
        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var appKeyConfig = await _context.AppKeyConfig.SingleOrDefaultAsync(m => m.Id == id);
            if (appKeyConfig == null)
            {
                return NotFound();
            }
            return View(appKeyConfig);
        }

        // POST: AppKeyConfigs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long id, [Bind("Id,BulkSmsUrl,BulkSmsUser,BulkSmsPassword,BulkSmsSid")] AppKeyConfig appKeyConfig)
        {
            if (id != appKeyConfig.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(appKeyConfig);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AppKeyConfigExists(appKeyConfig.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(appKeyConfig);
        }

        // GET: AppKeyConfigs/Delete/5
        public async Task<IActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var appKeyConfig = await _context.AppKeyConfig
                .SingleOrDefaultAsync(m => m.Id == id);
            if (appKeyConfig == null)
            {
                return NotFound();
            }

            return View(appKeyConfig);
        }

        // POST: AppKeyConfigs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(long id)
        {
            var appKeyConfig = await _context.AppKeyConfig.SingleOrDefaultAsync(m => m.Id == id);
            _context.AppKeyConfig.Remove(appKeyConfig);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool AppKeyConfigExists(long id)
        {
            return _context.AppKeyConfig.Any(e => e.Id == id);
        }
    }
}

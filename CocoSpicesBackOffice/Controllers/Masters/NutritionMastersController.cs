﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Entity.Entities;
using Entity.Data;
namespace CocoSpicesBackOffice.Controllers.Masters
{
    public class NutritionMastersController : Controller
    {
        private readonly ApplicationDbContext _context;

        public NutritionMastersController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: NutritionMasters
        public async Task<IActionResult> Index()
        {
            return View(await _context.NutritionMaster.ToListAsync());
        }

        // GET: NutritionMasters/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var nutritionMaster = await _context.NutritionMaster
                .SingleOrDefaultAsync(m => m.Id == id);
            if (nutritionMaster == null)
            {
                return NotFound();
            }

            return View(nutritionMaster);
        }

        // GET: NutritionMasters/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: NutritionMasters/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Title,Active")] NutritionMaster nutritionMaster)
        {
            if (ModelState.IsValid)
            {
                _context.Add(nutritionMaster);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(nutritionMaster);
        }

        // GET: NutritionMasters/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var nutritionMaster = await _context.NutritionMaster.SingleOrDefaultAsync(m => m.Id == id);
            if (nutritionMaster == null)
            {
                return NotFound();
            }
            return View(nutritionMaster);
        }

        // POST: NutritionMasters/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Title,Active")] NutritionMaster nutritionMaster)
        {
            if (id != nutritionMaster.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(nutritionMaster);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!NutritionMasterExists(nutritionMaster.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(nutritionMaster);
        }

        // GET: NutritionMasters/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var nutritionMaster = await _context.NutritionMaster
                .SingleOrDefaultAsync(m => m.Id == id);
            if (nutritionMaster == null)
            {
                return NotFound();
            }

            return View(nutritionMaster);
        }

        // POST: NutritionMasters/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var nutritionMaster = await _context.NutritionMaster.SingleOrDefaultAsync(m => m.Id == id);
            _context.NutritionMaster.Remove(nutritionMaster);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool NutritionMasterExists(int id)
        {
            return _context.NutritionMaster.Any(e => e.Id == id);
        }
    }
}

﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Entity.Entities;
using Entity.Data;
namespace CocoSpicesBackOffice.Controllers.Masters
{
    public class StatusMastersController : Controller
    {
        private readonly ApplicationDbContext _context;

        public StatusMastersController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: StatusMasters
        public async Task<IActionResult> Index()
        {
            return View(await _context.StatusMaster.ToListAsync());
        }

        // GET: StatusMasters/Details/5
        public async Task<IActionResult> Details(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var statusMaster = await _context.StatusMaster
                .SingleOrDefaultAsync(m => m.Id == id);
            if (statusMaster == null)
            {
                return NotFound();
            }

            return View(statusMaster);
        }

        // GET: StatusMasters/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: StatusMasters/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Title,Active")] StatusMaster statusMaster)
        {
            if (ModelState.IsValid)
            {
                _context.Add(statusMaster);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(statusMaster);
        }

        // GET: StatusMasters/Edit/5
        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var statusMaster = await _context.StatusMaster.SingleOrDefaultAsync(m => m.Id == id);
            if (statusMaster == null)
            {
                return NotFound();
            }
            return View(statusMaster);
        }

        // POST: StatusMasters/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long id, [Bind("Id,Title,Active")] StatusMaster statusMaster)
        {
            if (id != statusMaster.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(statusMaster);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!StatusMasterExists(statusMaster.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(statusMaster);
        }

        // GET: StatusMasters/Delete/5
        public async Task<IActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var statusMaster = await _context.StatusMaster
                .SingleOrDefaultAsync(m => m.Id == id);
            if (statusMaster == null)
            {
                return NotFound();
            }

            return View(statusMaster);
        }

        // POST: StatusMasters/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(long id)
        {
            var statusMaster = await _context.StatusMaster.SingleOrDefaultAsync(m => m.Id == id);
            _context.StatusMaster.Remove(statusMaster);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool StatusMasterExists(long id)
        {
            return _context.StatusMaster.Any(e => e.Id == id);
        }
    }
}

﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Entity.Entities;
using Entity.Data;
namespace CocoSpicesBackOffice.Controllers.Masters
{
    public class ModeOfPaymentMastersController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ModeOfPaymentMastersController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: ModeOfPaymentMasters
        public async Task<IActionResult> Index()
        {
            return View(await _context.ModeOfPaymentMaster.ToListAsync());
        }

        // GET: ModeOfPaymentMasters/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var modeOfPaymentMaster = await _context.ModeOfPaymentMaster
                .SingleOrDefaultAsync(m => m.Id == id);
            if (modeOfPaymentMaster == null)
            {
                return NotFound();
            }

            return View(modeOfPaymentMaster);
        }

        // GET: ModeOfPaymentMasters/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: ModeOfPaymentMasters/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Title,Active")] ModeOfPaymentMaster modeOfPaymentMaster)
        {
            if (ModelState.IsValid)
            {
                _context.Add(modeOfPaymentMaster);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(modeOfPaymentMaster);
        }

        // GET: ModeOfPaymentMasters/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var modeOfPaymentMaster = await _context.ModeOfPaymentMaster.SingleOrDefaultAsync(m => m.Id == id);
            if (modeOfPaymentMaster == null)
            {
                return NotFound();
            }
            return View(modeOfPaymentMaster);
        }

        // POST: ModeOfPaymentMasters/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Title,Active")] ModeOfPaymentMaster modeOfPaymentMaster)
        {
            if (id != modeOfPaymentMaster.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(modeOfPaymentMaster);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ModeOfPaymentMasterExists(modeOfPaymentMaster.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(modeOfPaymentMaster);
        }

        // GET: ModeOfPaymentMasters/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var modeOfPaymentMaster = await _context.ModeOfPaymentMaster
                .SingleOrDefaultAsync(m => m.Id == id);
            if (modeOfPaymentMaster == null)
            {
                return NotFound();
            }

            return View(modeOfPaymentMaster);
        }

        // POST: ModeOfPaymentMasters/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var modeOfPaymentMaster = await _context.ModeOfPaymentMaster.SingleOrDefaultAsync(m => m.Id == id);
            _context.ModeOfPaymentMaster.Remove(modeOfPaymentMaster);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ModeOfPaymentMasterExists(int id)
        {
            return _context.ModeOfPaymentMaster.Any(e => e.Id == id);
        }
    }
}

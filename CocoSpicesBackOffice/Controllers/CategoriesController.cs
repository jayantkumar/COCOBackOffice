﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Entity.Entities;
using Entity.Data;

namespace CocoSpicesBackOffice.Controllers
{
    public class CategoriesController : Controller
    {
        private ApplicationDbContext _db;
       public CategoriesController(ApplicationDbContext db)
        {
            _db = db;
        }
        [HttpGet("Create")]
        public IActionResult Create()
        {
            Category category = new Category();
            BindParents bindParents = new BindParents();
            bindParents.BindList = new Microsoft.AspNetCore.Mvc.Rendering.SelectList(_db.Category.ToList(), "Id", "CategoryTitle");
            ViewBag.ParentId = bindParents.BindList;
            return View();
        }
        [HttpPost("Create")]   
        public IActionResult Create(Category category)
        {
            try
            {
                if(category.ParentId==null)
                {
                    category.ParentId = 0;
                }
                _db.Category.Add(category);
                _db.SaveChanges();
            }
            catch(Exception ex)
            {
                throw ex;
            }
            
            return View();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Entity.Entities;
using Entity.Data;

namespace CocoSpicesBackOffice.Controllers
{
    public class ProductsController : Controller
    {
        private ApplicationDbContext _db;
        public ProductsController(ApplicationDbContext db)
        {
            _db = db;
        }
        [HttpGet]
        public IActionResult Create()
        {
            List<Product> prodcts = new List<Product>();
            try
            {
                
                Category category = new Category();
                StatusMaster statusMaster = new StatusMaster();
                BindParents bindParents = new BindParents();
                bindParents.BindList = new Microsoft.AspNetCore.Mvc.Rendering.SelectList(_db.Category.ToList(), "Id", "CategoryTitle");
                ViewBag.CategoryId = bindParents.BindList;
                bindParents.BindList = new Microsoft.AspNetCore.Mvc.Rendering.SelectList(_db.StatusMaster.ToList(), "Id", "Title");
                ViewBag.Status = bindParents.BindList;
                ViewBag.ProductId= new Microsoft.AspNetCore.Mvc.Rendering.SelectList(_db.Product.ToList(), "Id", "ProductName");
                //BindPartialView();
                prodcts = _db.Product.ToList();
                ViewBag.data = prodcts;
            }
            catch(Exception ex)
            {
                return View();
            }

            return View();
        }
        [HttpPost("Products/Create")]
        public IActionResult Create(Product product)
        {
           List< Product> prodcts = new  List<Product>();
            try
            {
                _db.Product.Add(product);
                _db.SaveChanges();
                prodcts = _db.Product.ToList();
            }
            catch (Exception ex)
            {
                return View();
            }

            return PartialView("_ProductDetails", prodcts);
        }
        public IActionResult BindPartialView(string param)
        {
            List<Product> prodcts = new List<Product>();
            List<ProductPicture> productPicture = new List<ProductPicture>();
            List<ProductNutrition> productNutrition = new List<ProductNutrition>();
            List<Unit> unit = new List<Unit>();
            prodcts = _db.Product.ToList();
            if (param== "Product")
            {
                
                prodcts = _db.Product.ToList();
                return PartialView("_ProductDetails", prodcts);
            }
           else if (param == "ProductPic")
            {

                productPicture = _db.ProductPicture.ToList();
                return PartialView("ProdctPicDetails", productPicture);
            }
            else if (param == "ProductNutrition")
            {

                productNutrition = _db.ProductNutrition.ToList();
                return PartialView("_ProdctNutrinDetails", productNutrition);
            }
            else if (param == "ProductUnit")
            {

                unit = _db.Unit.ToList();
                return PartialView("_ProdctUnitDetails", unit);
            }

            
            return PartialView("_ProductDetails", prodcts);
        }
        public IActionResult AddPicture(ProductPicture productPicture)
        {
            _db.ProductPicture.Add(productPicture);
            return View();
        }
        public IActionResult AddProduct(Product product)
        {
            _db.Product.Add(product);
            return View();
        }
        public IActionResult AddProductPic(ProductPicture productPicture)
        {
            _db.ProductPicture.Add(productPicture);
            return View();
        }
    }
}